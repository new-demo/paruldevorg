public class Foo {

    private String name;
    public Foo(String name){
        this.name = name;
    }

    public String helloWorld(){
        return 'hello This is the Sample Demo  lass in the name of Foo' + name + ' from Bitnucket SFDX pipelines';
    }

}